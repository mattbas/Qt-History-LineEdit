/*
 * SPDX-FileCopyrightText: 2012 Mattia Basaglia <dev@dragon.best>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <QApplication>
#include "history_line_edit.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    HistoryLineEdit d;
    d.show();
    
    return a.exec();
}
