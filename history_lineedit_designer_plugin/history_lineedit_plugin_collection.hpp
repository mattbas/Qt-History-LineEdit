/*
 * SPDX-FileCopyrightText: 2012 Mattia Basaglia <dev@dragon.best>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef HISTORY_LINE_EDIT_PLUGIN_COLLECTION_HPP
#define HISTORY_LINE_EDIT_PLUGIN_COLLECTION_HPP

#include <QDesignerCustomWidgetCollectionInterface>

class History_LineEdit_Plugin_Collection : public QObject, public QDesignerCustomWidgetCollectionInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "mattia.basaglia.History_LineEdit_Plugin")
    Q_INTERFACES(QDesignerCustomWidgetCollectionInterface)

public:
    explicit History_LineEdit_Plugin_Collection(QObject *parent = 0);

    QList<QDesignerCustomWidgetInterface*> customWidgets() const;

   private:
       QList<QDesignerCustomWidgetInterface*> widgets;
    
};

#endif // HISTORY_LINE_EDIT_PLUGIN_COLLECTION_HPP
