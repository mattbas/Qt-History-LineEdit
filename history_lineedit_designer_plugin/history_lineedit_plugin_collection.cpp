/*
 * SPDX-FileCopyrightText: 2012 Mattia Basaglia <dev@dragon.best>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "history_lineedit_plugin_collection.hpp"
#include "history_lineedit_plugin.hpp"

History_LineEdit_Plugin_Collection::History_LineEdit_Plugin_Collection(QObject *parent) :
    QObject(parent)
{
    widgets.push_back(new History_LineEdit_Plugin(this));
}

QList<QDesignerCustomWidgetInterface *> History_LineEdit_Plugin_Collection::customWidgets() const
{
    return widgets;
}
