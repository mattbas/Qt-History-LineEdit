# SPDX-FileCopyrightText: 2012 Mattia Basaglia <dev@dragon.best>
# SPDX-License-Identifier: BSD-2-Clause

INCLUDEPATH += $$PWD/src

SOURCES += \
    $$PWD/src/history_line_edit.cpp

HEADERS += \
    $$PWD/src/history_line_edit.hpp


